// Copyright (c) 2024 Deltaman group limited. All rights reserved.
export enum InputId {
    A = "A",
    B = "B"
}

export enum ActionType {
    ThreeWayMerge,
    Graft,
    Compare,
    TwoWayMerge,
    Patch,
    Default
}

export interface DeltaJsonRoot {
    dx_deltaJSON: DeltaJson;
}

export interface DeltaJson {
    dx_data_sets: string,
    dx_deltaJSON_type: string,
    dx_deltaJSON_metadata: any,
    dx_deltaJSON_delta: any
}

export interface DeltaJsonMetadata {
    operation: DeltaOperation,
    parameters: DeltaParameters
}

export interface DeltaOperation {
    type: string,
    "input-format": string,
    "output-format": string
}

interface DeltaParameters {
    arrayAlignment: string;
    wordByWord: boolean;
}

export class DeltaProcessor {

    public static get DX_DELTA(): string { return "dx_delta"; }
    public static get DX_DELTA_STRING(): string { return "dx_delta_string"; }

    public extractFromDeltaJSON(targetCode: InputId, source: DeltaJsonRoot): any {

        let extractedJSON = this.performExtract(targetCode, source);
        return extractedJSON.dx_deltaJSON.dx_deltaJSON_delta;
    }
    
    private performExtract(targetCode: InputId, source: DeltaJsonRoot, destination?: any): DeltaJsonRoot {

        if (!destination) {
            destination = source;
            if (source) {
                let sourceType = DeltaProcessor.getJsonItemType(source);
                if (sourceType == "array") {
                    destination = this.performExtract(targetCode, source, []);
                } else if (sourceType == "object") {
                    if (DeltaProcessor.isDelta(source)) {
                        let deltaData = DeltaProcessor.getDeltaDataByTargetCode(targetCode, source);
                        if (deltaData !== undefined)
                            destination = DeltaProcessor.deepCopy(deltaData);
                    }
                    else {
                        destination = this.performExtract(targetCode, source, {});
                    }
                } else {
                    destination = source;
                }
            } 
        } else {
            if (source === destination) throw Error("extractFromFullContext - Source and destination are identical");

            if (Array.isArray(source)) {
                destination.length = 0;
                for (let i = 0; i < source.length; i++) {
                    let childSource = source[i];
                    let childSourceType = DeltaProcessor.getJsonItemType(childSource);

                    if (childSourceType == "object" && DeltaProcessor.isDelta(childSource)) {
                        let deltaData = DeltaProcessor.getDeltaDataByTargetCode(targetCode, childSource);
                        if (deltaData !== undefined)
                            destination.push(DeltaProcessor.deepCopy(deltaData));
                    }
                    else {
                        destination.push(this.performExtract(targetCode, childSource));
                    }
                }
            } else {

                for (let key in source) {
                    let childSource = source[key];
                    let childSourceType = DeltaProcessor.getJsonItemType(childSource);

                    if (childSourceType == "object" && DeltaProcessor.isDelta(childSource)) {
                        let deltaData = DeltaProcessor.getDeltaDataByTargetCode(targetCode, childSource);
                        if (deltaData !== undefined)
                            destination[key] = DeltaProcessor.deepCopy(deltaData);
                    }
                    else {
                        destination[key] = this.performExtract(targetCode, childSource);
                    }
                }
            }
        }

        return destination;
    }

    private static getJsonItemType(x: any): string {
        if (typeof x == "string")
            return "string";

        if (typeof x == "number")
            return "number";

        if (typeof x == "boolean")
            return "boolean";

        if (x === null)
            return "null";

        if (Array.isArray(x))
            return "array";

        if (x instanceof Object)
            return "object";

        //if ( this.item === undefined || typeof this.item == "function" ) {}

        return "unsupported";
    }

    private static deepCopy(source: any, destination?: any) : any {

        if (!destination) {
            destination = source;
            if (source) {
                var sourceType = DeltaProcessor.getJsonItemType(source);
                if (sourceType == "array") {
                    destination = DeltaProcessor.deepCopy(source, []);
                } else if (sourceType == "object") {
                    destination = DeltaProcessor.deepCopy(source, {});
                } else {
                    destination = source;
                }
            }
        } else {
            if (source === destination) throw Error("Can't deepCopy - Source and destination are identical");
            if (Array.isArray(source)) {
                destination.length = 0;
                for (var i = 0; i < source.length; i++) {
                    destination.push(DeltaProcessor.deepCopy(source[i]));
                }
            } else {
                for (var key in source) {
                    destination[key] = DeltaProcessor.deepCopy(source[key]);
                }
            }
        } return destination;
    }

    private static getDeltaDataByTargetCode(targetCode: InputId, sourceObj: any): any {

        if (sourceObj.hasOwnProperty(DeltaProcessor.DX_DELTA)) {

            let deltaObj = sourceObj[DeltaProcessor.DX_DELTA];
            if (deltaObj.hasOwnProperty(targetCode))
                return deltaObj[targetCode];

        }
        else if (sourceObj.hasOwnProperty(DeltaProcessor.DX_DELTA_STRING)) {

            // Build the string from the delta-string array based on the target code
            let deltaStringArr = sourceObj[DeltaProcessor.DX_DELTA_STRING];
            let strArr = new Array<string>();

            for (let i = 0; i < deltaStringArr.length; i++) {
                let childSource = deltaStringArr[i];
                let childSourceType = DeltaProcessor.getJsonItemType(childSource);

                if (childSourceType == "object" && DeltaProcessor.isDelta(childSource)) {
                    let deltaData = DeltaProcessor.getDeltaDataByTargetCode(targetCode, childSource);
                    if (deltaData !== undefined)
                        strArr.push(deltaData.toString());
                }
                else {
                    strArr.push(childSource.toString());
                }
            }

            return strArr.join("");
        }

        // Valid to return undefined meaning the delta doesnt contain the specified targetcode A or B
        return undefined;
    }

    private static isDelta(sourceObj: any): boolean {
        return sourceObj.hasOwnProperty(DeltaProcessor.DX_DELTA) ||
            sourceObj.hasOwnProperty(DeltaProcessor.DX_DELTA_STRING);
    }
}