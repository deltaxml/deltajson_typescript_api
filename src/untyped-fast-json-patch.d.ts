// Copyright (c) 2024 Deltaman group limited. All rights reserved.
declare module "node_modules/fast-json-patch/dist/fast-json-patch.js";
declare var jsonpatch: any;
declare function applyPatch(source: any, patch: any): any;