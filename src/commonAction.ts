// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonConfig, ProgressListenerFunction, RequestStatus } from "./commonConfig";

export abstract class CommonAction {

  public static readonly jobProgressProperties = [
    'progressInPercentage',
    'pipelineStage',
    'jobStatus']

  static readonly fileFieldA = "ajson";
  static readonly fileFieldB = "bjson";
  static readonly fileFieldChangeSet = "changeset";
  static readonly fileFieldTarget = "target";

  baseURL: string;
  commonURL: string;
  internalState = "READY";
  intervalId: any = null;
  progressListener: ProgressListenerFunction;
  pollingIntervalDefault = 500;
  pollingInterval = this.pollingIntervalDefault;
  maxPollNumber = 1000;
  pollCounter = 0;


  jobUrl: string;
  downloadUrl: string;

  constructor(baseURL: string, urlPart: string) {
    let interimUrl = baseURL.endsWith('/') ? baseURL : baseURL + '/';
    this.baseURL = interimUrl;
    this.commonURL = this.baseURL + urlPart;
    this.pollingInterval = this.pollingIntervalDefault;
  }

  downloadRequest = (resolve: Function, reject: Function) => {
    // download request function
    fetch(this.downloadUrl, {
      method: 'GET',
      headers: { 'Accept': 'application/json' }
    })
      .then(res => res.json())
      .then((data) => {
        resolve(data);
        this.setComparisonStatus('COMPLETED', null);
      })
      .catch((error) => { reject(error) })
  }

  jobRequest = (resolve: Function, reject: Function) => {
    // job request function
    this.pollCounter++;
    if (this.pollCounter > this.maxPollNumber) {
      clearInterval(this.intervalId);
      reject({ errorStatus: 'Max poll number exceeded: ' + this.pollCounter });
    }
    fetch(this.jobUrl, {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(res => res.json())
      .then((data) => {
        this.setComparisonStatus(data.jobStatus, data);
        if (data.jobStatus === RequestStatus.finished) {
          this.downloadUrl = this.baseURL + "downloads/" + data.jobId;
          clearInterval(this.intervalId);
          this.downloadRequest(resolve, reject);
        } else if (data.jobStatus === RequestStatus.failed) {
          clearInterval(this.intervalId);
          reject(data);
        }
      })
      .catch((error) => reject(error))
  }

  handleResponseObject = (data: any, resolve: Function, reject: Function) => {
    this.pollCounter = 0;
    if (data.errorCode) {
      throw data
    } else if (data.jobStatus === RequestStatus.submitted || data.jobStatus === RequestStatus.started) {
      this.setComparisonStatus(data.jobStatus, data);
      this.jobUrl = this.baseURL + "jobs/" + data.jobId;
      this.intervalId = setInterval(() => { this.jobRequest(resolve, reject) }, this.pollingInterval); // end setInterval

    } // end if data.jobStatus
  }

  public addProgressListener(progressListener: ProgressListenerFunction) {
    this.progressListener = progressListener;
    return this;
  }   

  // valid values: 0.1 seconds to 10 seconds
  public setPollingInterval(interval: number) {
    if (interval > 99 && interval < 10001) {
      this.pollingInterval = interval;
    }
    return this;
  }

  public setMaxPollNumber(pollNumber: number) {
    if (pollNumber > 0) {
      this.maxPollNumber = pollNumber;
    }
    return this;
  }

  setComparisonStatus(statusMessage: string, statusObject: any) {
    // show status
    if (this.progressListener) {
      this.progressListener(statusMessage, statusObject);
    }
  }

  public action = (commonData: CommonConfig): Promise<any> => {

    return new Promise((resolve, reject) => {
      this.setComparisonStatus("REQUESTING...", null);
      let formData = new FormData();
      commonData.populateFormData(formData);
      formData.append('async', 'true');

      fetch(this.commonURL, {
        method: "POST",
        headers: {
          "Accept": "application/json"
        },
        body: formData
      }).then(res => res.json())
        .then((data) => { this.handleResponseObject(data, resolve, reject) })
        .catch((error) => {
          if (error.errorCode) {
            reject(error);
          } else {
            reject({
              errorMessage: error.message,
              URL: this.commonURL
            })
          }
        })
    });
  }
}