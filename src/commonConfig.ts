// Copyright (c) 2024 Deltaman group limited. All rights reserved.
export abstract class CommonConfig implements CommonRestProperties {
  patchDirection?: PatchDirection;
  outputmodeGraft?: GraftResolutionMode;
  arrayAlignment?: ArrayAlignment;
  target?: File;
  changeSet?: File;
  threeWayMergeMode?: ThreeWayMergeMode;
  ancestorFile?: File;
  edit1File?: File;
  edit2File?: File;
  dxConfiguration?: DXConfiguration = { dxConfig: [] };
  wordByWord?: boolean;
  restNames: CommonDataOrName;
  aJSON?: File;
  bJSON?: File;
  mergePriority?: MergePriority;

  constructor(){}

  public addIgnoreChanges(key: string): CommonConfig {
    this.dxConfiguration.dxConfig.push({
      key: key,
      ignoreChanges: IgnoreChangeType.delete
    });
    return this;
  }

  public clearIgnoreChanges() {
    this.dxConfiguration.dxConfig = [];
    return this;
  }

  /**
   * Set the array alignment using the values from the enum ArrayAlignment.
   * 
   * @remarks 
   * This is a commonAction function accessible via all action classes.
   * 
   * @param arrayAlignment - Array alignment type to be set
   * @returns Set the array alignment for the desired action
   */
  public setArrayAlignment(arrayAlignment: ArrayAlignment) {
    this.arrayAlignment = arrayAlignment;
    return this;
  }


  public setWordByWord(wordByWord: boolean) {
    this.wordByWord = wordByWord;
    return this;
  }

  public populateFormData(data: FormData): FormData {
    for (const key of Object.keys(this.restNames)) {
      data.append(this.restNames[key], this[key]);
    }
    return data;
  }

  public setMergePriority(mergePriority: MergePriority) {
      this.mergePriority = mergePriority;
      return this;
  }

  public setOutputModeGraft(outputModeGraft: GraftResolutionMode) {
    this.outputmodeGraft = outputModeGraft;
    return this;
  }

  public setOutputFormat(outputFormat: ThreeWayMergeMode) {
    this.threeWayMergeMode = outputFormat;
    return this;
  }

  public setPatchDirection(patchDirection: PatchDirection) {
    this.patchDirection = patchDirection;
    return this;
  }
}

// authoratative interface
export interface CommonDataOrName {
  arrayaAlignment?: ArrayAlignment | string,
  dxConfiguration?: DXConfiguration | string,
  aJSON?: File | string,
  bJSON?: File | string,
  changeSet?: File | string,
  target?: File | string,
  wordByWord?: boolean | string,
  patchDirection?: PatchDirection | string,
  graftResolutionMode?: GraftResolutionMode | string,
  threeWayMergeMode?: ThreeWayMergeMode | string;
  ancestorFile?: File | string;
  edit1File?: File | string;
  edit2File?: File | string;
}

export abstract class CommonRestProperties implements CommonDataOrName {
  arrayaAlignment?: ArrayAlignment;
  dxConfiguration?: DXConfiguration;
  aJSON?: File;
  bJSON?: File;
  changeSet?: File;
  target?: File
  wordByWord?: boolean;
  patchDirection?: PatchDirection;
  graftResolutionMode?: GraftResolutionMode;
  threeWayMergeMode?: ThreeWayMergeMode;
  ancestorFile?: File;
  edit1File?: File;
  edit2File?: File;
}

export enum ArrayAlignment {
  positionPriority = "positionPriority",
  typeWithValuePriority = "typeWithValuePriority",
  orderless = "orderless"
}

export enum GraftResolutionMode {
  changesetPriopity = "changesetPriority",
  targetPririty = "targetPriority",
  additionOnly = "additionsOnly"
}

interface DXConfiguration {
  dxConfig: Array<IgnoreChanges>
}

interface IgnoreChanges {
  ignoreChanges: IgnoreChangeType,
  key: string
}

enum IgnoreChangeType {
  delete = "delete"
}

export enum MergePriority {
  a = "a",
  b = "b" 
}

export enum RequestStatus {
  submitted = "SUBMITTED",
  started = "STARTED",
  finished = "FINISHED",
  failed = "FAILED"
}

export enum PatchDirection {
  aTOb = "aToB",
  bToa = "bToA"
}

export enum ThreeWayMergeMode {
  showConflicts = "showConflicts",
  resolveUsingEdit1 = "resolvedUsingEdit1",
  resolveUsingEdit2 = "resolvedUsingEdit2"
}

export type ProgressListenerFunction = (statusText: string, statusObject: any) => void;