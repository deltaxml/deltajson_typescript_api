// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonConfig, ArrayAlignment, CommonDataOrName, CommonRestProperties } from "./commonConfig";
import { CommonAction } from "./commonAction";

export class CompareConfig extends CommonConfig implements CompareRestProperties {
  arrayAlignment: ArrayAlignment.typeWithValuePriority;
  wordByWord = true;
  restNames = new CompareRestPropertyNames();

  constructor(aFile: File | any, bFile: File | any) {
    super();
    this.aJSON = aFile;
    this.bJSON = bFile;
  }
}

interface CompareDataOrName extends CommonDataOrName {
  arrayAlignment: ArrayAlignment | string;
  wordByWord: boolean | string;
}

class CompareRestProperties extends CommonRestProperties implements CompareDataOrName{
  arrayAlignment: ArrayAlignment;
  wordByWord: boolean;
}

export class CompareRestPropertyNames implements CompareDataOrName {
  public readonly dxConfiguration: "dxConfig";
  public readonly arrayAlignment = "arrayAlignment";
  public readonly wordByWord = "wordByWord";
  public readonly aJSON = "a";
  public readonly bJSON = "b";
}

export class CompareAction extends CommonAction {

  constructor(baseURL: string) {
    super(baseURL, "compare");
  }
  
  public compare = this.action;
}