// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonConfig, GraftResolutionMode, CommonDataOrName, CommonRestProperties } from "./commonConfig";
import { CommonAction } from "./commonAction";

export class GraftConfig extends CommonConfig implements GraftRestProperties {
    public graftResolutionMode = GraftResolutionMode.changesetPriopity;
    restNames = new GraftRestPropertyNames();

    constructor(aFile: File | any, bFile: File | any) {
      super();
      this.changeSet = aFile;
      this.target = bFile;
    }
}

interface GraftDataOrName extends CommonDataOrName {
  graftResolutionMode: GraftResolutionMode | string;
}

class GraftRestProperties extends CommonRestProperties implements GraftDataOrName{
  graftResolutionMode: GraftResolutionMode;
}

export class GraftRestPropertyNames implements GraftDataOrName {
  public readonly graftResolutionMode = "graftResolutionMode";
  public readonly changeSet = "changeset";
  public readonly target = "target";
}

export class GraftAction extends CommonAction {

  constructor(baseURL: string) {
    super(baseURL, "graft");
  }

  public graft = this.action;
}