// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonConfig, ArrayAlignment, MergePriority, CommonDataOrName, CommonRestProperties } from "./commonConfig";
import { CommonAction } from "./commonAction";

export class TwoWayMergeConfig extends CommonConfig implements TwoWayMergeRestProperties {
  arrayAlignment: ArrayAlignment;
  public mergePriority = MergePriority.a;
  public ArrayAlignment = ArrayAlignment.orderless;
  public wordByWord = true;
  restNames = new TwoWayMergeRestPropertyNames();

  constructor(aFile: File | any, bFile: File | any) {
    super();
    this.aJSON = aFile;
    this.bJSON = bFile;
  }
}

interface TwoWayMergeDataOrName extends CommonDataOrName {
  mergePriority: MergePriority | string;
  arrayAlignment: ArrayAlignment | string;
  wordByWord: boolean | string;
}

class TwoWayMergeRestProperties extends CommonRestProperties implements TwoWayMergeDataOrName{
  mergePriority: MergePriority;
  arrayAlignment: ArrayAlignment;
  wordByWord: boolean;
}

export class TwoWayMergeRestPropertyNames implements TwoWayMergeDataOrName {
  public readonly mergePriority = "mergePriority";
  public readonly dxConfiguration: "dxConfig";
  public readonly arrayAlignment = "arrayAlignment";
  public readonly wordByWord = "wordByWord";
  public readonly aJSON = "a";
  public readonly bJSON = "b";
}
  
export class TwoWayMergeAction extends CommonAction {

  constructor(baseURL: string) {
    super(baseURL, "two-way-merge");
  }

  public merge = this.action;
}