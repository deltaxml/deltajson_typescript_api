// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CompareAction, CompareConfig} from './compareAction';
import { TwoWayMergeAction, TwoWayMergeConfig} from './twoWayMergeAction';
import { CreatePatchAction, CreatePatchConfig} from './patchAction';
import { GraftAction, GraftConfig} from './graftAction';
import { ThreeWayMergeAction, ThreeWayMergeConfig} from './threeWayMergeAction';
import { ArrayAlignment, MergePriority, ThreeWayMergeMode, GraftResolutionMode, PatchDirection} from './commonConfig';
import { DeltaProcessor, InputId, ActionType} from './deltaProcessor';

let inputElementFileA: HTMLInputElement;
let inputElementFileB: HTMLInputElement;
let inputElementAncestor: HTMLInputElement;
let inputElementEdit1: HTMLInputElement;
let inputElementEdit2: HTMLInputElement;
let inputElementChangeset: HTMLInputElement;
let inputElementTarget: HTMLInputElement;
let statusSpan: HTMLElement;
let logSpan: HTMLElement;
let resultElement: HTMLElement;
let extractArea: HTMLElement;
let actions: HTMLSelectElement;
let extractPriority: HTMLSelectElement;
let threeFiles: HTMLElement;
let twoFiles: HTMLElement;
let extractButton: HTMLInputElement;
let applyPatchButton: HTMLInputElement;
let graftFiles: HTMLElement;
let currentActionType: ActionType;
let execute: HTMLElement;
let cachedCompareResult: any;
let cachedCreatePatchResultAToB: any;
let cachedFileAFromCreatePatch: File|null = null;

let settings = {
    deltaJSON: "http://0.0.0.0:8080/api/json/v1",
    jsonIndent: 2,
    statusPollingInterval: 100,
    maxPollNumber: 1000,
}

function initialiseElements() {
    inputElementFileA = <HTMLInputElement>document.getElementById('fileA');
    inputElementFileB = <HTMLInputElement>document.getElementById('fileB');
    inputElementAncestor = <HTMLInputElement>document.getElementById('ancestor');
    inputElementEdit1 = <HTMLInputElement>document.getElementById('edit1');
    inputElementEdit2 = <HTMLInputElement>document.getElementById('edit2');
    inputElementChangeset = <HTMLInputElement>document.getElementById('changeset');
    inputElementTarget = <HTMLInputElement>document.getElementById('target');
    extractButton = <HTMLInputElement>document.getElementById('extract');
    applyPatchButton = <HTMLInputElement>document.getElementById('applyPatch');

    statusSpan = document.getElementById('status');
    logSpan = document.getElementById('log-result');
    actions = <HTMLSelectElement>document.getElementById('actions');
    extractPriority = <HTMLSelectElement>document.getElementById('extractPriority');
    threeFiles = document.getElementById('threeFiles');
    twoFiles = document.getElementById('twoFiles');
    graftFiles = document.getElementById('graftFiles');
    execute = document.getElementById('execute');

    resultElement = document.getElementById('json-result');
    extractArea = document.getElementById('processor-result');
    actions.addEventListener('change', switchDisplayedFileBar);
    execute.addEventListener('click', executeActions);
    extractButton.addEventListener('click', extractJSON);
    applyPatchButton.addEventListener('click', applyPatchToJSON);
}

let progressListener = (statusInfo: string, statusObject: any) => {
    statusSpan.textContent = statusInfo;
    if (statusObject) {
        logSpan.append(JSON.stringify(statusObject, CompareAction.jobProgressProperties, settings.jsonIndent) + '\n\n');
        logSpan.scrollTop = logSpan.scrollHeight - logSpan.clientHeight;
    }
}

function switchDisplayedFileBar() {
    let selOptIndex = actions.selectedIndex;
    let opt = actions.options[selOptIndex];
    currentActionType = ActionType[opt.value];
    switch(currentActionType) {
        case ActionType.ThreeWayMerge:
            threeFiles.style.display = 'block';
            twoFiles.style.display = 'none';
            graftFiles.style.display = 'none';
            extractButton.style.display = 'none';
            applyPatchButton.style.display = 'none';
            extractPriority.style.display = 'none';
            
            break;
        case ActionType.Graft:
            threeFiles.style.display = 'none';
            twoFiles.style.display = 'none';
            graftFiles.style.display = 'block';
            extractButton.style.display = 'none';
            applyPatchButton.style.display = 'none';
            extractPriority.style.display = 'none';
            break;
        case ActionType.Compare:
            extractButton.style.display = 'inline-block';
            applyPatchButton.style.display = 'none';
            extractPriority.style.display = 'inline-block';
            threeFiles.style.display = 'none';
            twoFiles.style.display = 'block';
            graftFiles.style.display = 'none';
            break;
        case ActionType.TwoWayMerge:
        case ActionType.Patch:
            if (currentActionType === ActionType.Patch) {
                applyPatchButton.style.display = 'inline-block';
            } else {
                applyPatchButton.style.display = 'none';
            }
            extractButton.style.display = 'none';
            extractPriority.style.display = 'none';
            threeFiles.style.display = 'none';
            twoFiles.style.display = 'block';
            graftFiles.style.display = 'none';
            break;
        default:
            threeFiles.style.display = 'none';
            twoFiles.style.display = 'block';
            graftFiles.style.display = 'none';
            extractButton.style.display = 'none';
            applyPatchButton.style.display = 'none';
            extractPriority.style.display = 'none';
            break;
    }
}

function executeActions() {
    switch(currentActionType) {
        case ActionType.Compare:
            compareJSON();
            break;
        case ActionType.TwoWayMerge:
            twoWayMergeJSON();
            break;
        case ActionType.Patch:
            createPatchJSON();
            break;
        case ActionType.Graft:
            graftJSON();
            break;
        case ActionType.ThreeWayMerge:
            threeWayMergeJSON();
            break;
        default:
            resultElement.textContent = "Please select an operation.";
            console.warn("Please select an operation.");
    }
}

let updateStatusOnError = (error) => {
    logSpan.append(JSON.stringify(error, null, settings.jsonIndent) + '\n\n');
    logSpan.scrollTop = logSpan.scrollHeight - logSpan.clientHeight;
    statusSpan.textContent = "ERROR";  
}

function compareJSON() {
    // initialise CompareAction class
    let c = new CompareAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    // reset HTML page context
    logSpan.textContent = '';
    resultElement.textContent = '';
    let fA: File = inputElementFileA.files[0];
    let fB: File = inputElementFileB.files[0];

    // Setup configuration for the comparison
    let config = new CompareConfig(fA, fB);
    config
        .addIgnoreChanges('myke')
        .addIgnoreChanges('also')
        .setArrayAlignment(ArrayAlignment.typeWithValuePriority)
        .setWordByWord(true);
    
    // Invoke an asynchronous comparison
    c.compare(config).then((data) => {
        cachedCompareResult = data;
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
        extractArea.textContent = 'DeltaJSON Processor';
    })
    .catch((error) => updateStatusOnError(error));
}

function extractJSON() {
    let extractPriorityIndex = extractPriority.selectedIndex;
    let option = extractPriority.options[extractPriorityIndex];
    let currentValue = option.value;
    let dp = new DeltaProcessor();
    let result =  dp.extractFromDeltaJSON(InputId[currentValue], cachedCompareResult);
    extractArea.textContent = JSON.stringify(result, null, settings.jsonIndent);
}

function twoWayMergeJSON() {
    let c = new TwoWayMergeAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    logSpan.textContent = '';
    resultElement.textContent = '';

    let fA: File = inputElementFileA.files[0];
    let fB: File = inputElementFileB.files[0];
    let config = new TwoWayMergeConfig(fA, fB);
    config
        .addIgnoreChanges('myke')
        .addIgnoreChanges('also')
        .setWordByWord(true)
        .setArrayAlignment(ArrayAlignment.orderless)
        .setMergePriority(MergePriority.a);
    

    c.merge(config).then((data) => {
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
        extractArea.textContent = 'DeltaJSON Processor is not available for this functionality';
    })
    .catch((error) => updateStatusOnError(error));
}

function createPatchJSON() {
    let c = new CreatePatchAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    logSpan.textContent = '';
    resultElement.textContent = '';

    let fA: File = inputElementFileA.files[0];
    let fB: File = inputElementFileB.files[0];
    cachedFileAFromCreatePatch = fA;

    let config = new CreatePatchConfig(fA, fB);
    config
        .addIgnoreChanges('myke')
        .addIgnoreChanges('also')
        .setPatchDirection(PatchDirection.aTOb);
    

    c.createPatch(config).then((data) => {
        cachedCreatePatchResultAToB = data;
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
        extractArea.textContent = 'Click on ApplyPatch Button to use Patch created to convert File A to File B';
    })
    .catch((error) => updateStatusOnError(error));
}

function graftJSON() {
    let c = new GraftAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    logSpan.textContent = '';
    resultElement.textContent = '';

    let fA: File = inputElementChangeset.files[0];
    let fB: File = inputElementTarget.files[0];
    let config = new GraftConfig(fA, fB);
    config
        .setOutputModeGraft(GraftResolutionMode.changesetPriopity);
    

    c.graft(config).then((data) => {
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
        extractArea.textContent = 'DeltaJSON Processor is not available for this functionality';
    })
    .catch((error) => updateStatusOnError(error));
}

function threeWayMergeJSON() {
    let c = new ThreeWayMergeAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    logSpan.textContent = '';
    resultElement.textContent = '';

    let fA: File = inputElementAncestor.files[0];
    let fe1: File = inputElementEdit1.files[0];
    let fe2: File = inputElementEdit2.files[0];
    let config = new ThreeWayMergeConfig(fA, fe1, fe2);
    config
        .setArrayAlignment(ArrayAlignment.orderless)
        .setWordByWord(true)
        .setOutputFormat(ThreeWayMergeMode.showConflicts);
    

    c.threeWayMerge(config).then((data) => {
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
        extractArea.textContent = 'DeltaJSON Processor is not available for this functionality';
    })
    .catch((error) => updateStatusOnError(error));
}

function applyPatchToJSON(){
    if (cachedFileAFromCreatePatch) {
        let f: File = cachedFileAFromCreatePatch;
        let fr = new FileReader();
        fr.readAsText(f);
        fr.addEventListener('load', () => {
            let fileContents = fr.result;
            if (typeof fileContents === 'string') {
                let sourceObj = JSON.parse(fileContents);
                if (cachedCreatePatchResultAToB) {
                    let patchResultObj = jsonpatch.applyPatch(sourceObj, cachedCreatePatchResultAToB).newDocument;
                    try {
                        extractArea.textContent = JSON.stringify(patchResultObj, null, settings.jsonIndent);
                    } catch (e) {
                        extractArea.textContent = 'Error occurred when trying to serialize JSON object to a string';
                    }
                } else {
                    extractArea.textContent = `The 'Create Patch' operation must be successfully executed first`;
                }
            }
        });
    } else {
        extractArea.textContent = `The 'Create Patch' operation must be executed first`;
    }
}

initialiseElements();



