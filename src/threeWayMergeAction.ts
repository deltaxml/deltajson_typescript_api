// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonConfig, ThreeWayMergeMode, ArrayAlignment, CommonDataOrName, CommonRestProperties } from "./commonConfig";
import { CommonAction } from "./commonAction";

export class ThreeWayMergeConfig extends CommonConfig implements ThreeWayMergeRestProperties {
  public threeWayMergeMode = ThreeWayMergeMode.showConflicts;
  public arrayAlignment: ArrayAlignment.typeWithValuePriority;
  public wordByWord = true;
  restNames = new ThreeWayMergeRestPropertyNames();

  constructor(ancestor: File | any, edit1: File | any, edit2: File | any) {
    super();
    this.ancestorFile = ancestor;
    this.edit1File = edit1;
    this.edit2File = edit2;
  }
}
  
interface ThreeWayMergeDataOrName extends CommonDataOrName {
  threeWayMergeMode: ThreeWayMergeMode | string;
  arrayAlignment: ArrayAlignment | string;
  wordByWord: boolean | string;
}

class ThreeWayMergeRestProperties extends CommonRestProperties implements ThreeWayMergeDataOrName{
  threeWayMergeMode: ThreeWayMergeMode;
  arrayAlignment: ArrayAlignment;
  wordByWord: boolean;
}

export class ThreeWayMergeRestPropertyNames implements ThreeWayMergeDataOrName {
  public readonly threeWayMergeMode = "threeWayMergeMode";
  public readonly arrayAlignment = "arrayAlignment";
  public readonly wordByWord = "wordByWord";
  public readonly ancestorFile = "ancestor";
  public readonly edit1File = "edit1";
  public readonly edit2File = "edit2";
}

export class ThreeWayMergeAction extends CommonAction {

  constructor(baseURL: string) {
    super(baseURL, "three-way-merge");
  }

  public threeWayMerge = this.action;
}