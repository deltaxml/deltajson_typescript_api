// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import { CommonAction } from './commonAction';
import { CommonConfig, PatchDirection, CommonDataOrName, CommonRestProperties } from './commonConfig';

export class CreatePatchConfig extends CommonConfig implements PatchRestProperties {
  public patchDirection = PatchDirection.aTOb;
  restNames = new PatchRestPropertyNames();

  constructor(aFile: File | any, bFile: File | any) {
    super();
    this.aJSON = aFile;
    this.bJSON = bFile;
  }
}

interface PatchDataOrName extends CommonDataOrName {
  patchDirection: PatchDirection | string;
}
  
class PatchRestProperties extends CommonRestProperties implements PatchDataOrName{
  patchDirection: PatchDirection;
}
  
export class PatchRestPropertyNames implements PatchDataOrName {
  public readonly dxConfiguration: "dxConfig";
  public readonly patchDirection = "patchDirection";
  public readonly aJSON = "a";
  public readonly bJSON = "b";
}
  
export class CreatePatchAction extends CommonAction {
  
  constructor(baseURL: string) {
    super(baseURL, "patch");
  }
    
  public createPatch = this.action;
}