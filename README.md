# README #

### A TypeScript API for comparison-based operations on JSON ###

## Project Goals: ##
1.  A TypeScript API wrapper for invoking DeltaJSON REST Services
2.  A sample client web application demonstrating:

    1. Use of the TypeScript API
    2. Applying a DeltaJSON-created patch using the [fast-json-patch](https://www.npmjs.com/package/fast-json-patch) NPM module


## Project Setup ###

To run this project you will need the following products installed:

* [NodeJS](https://nodejs.org/en/) (Install NPM also)
* [TypeScript Compiler](https://www.typescriptlang.org/#download-links)
* [VSCode](https://code.visualstudio.com/download) (or alternative TypeScript editor)
* [DeltaJSON](https://www.deltaxml.com/products/compare/json-compare/) (license required)
* [Java 8](https://www.oracle.com/java/technologies/javase-jre8-downloads.html) (or later)
* [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) (recommended VSCode extension)
* [Chrome Debugger](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) (recommended VSCode extension)

## Project installation

1. Download or clone this project to your local machine
2. From the terminal, navigate to the project directory
3. Ensure all dependencies listed above are installed
4. From the terminal, run: ```npm install```

## To Run the sample client ###

* From the terminal, run ```npm run build```
    -> This will create an *out* directory in your project with all the .js files used by the sample application.
* Startup DeltaJSON REST server from the terminal:
    ```java -jar deltajson-rest-1.0.0.jar```
* Open the file ```index.html``` in your browser
* Select the DeltaJSON operation and JSON files you want to process
* Run the DeltaJSON operation

_Sample client code from main.ts_
``` javascript
let progressListener = (statusInfo: string, statusObject: any) => {
    statusSpan.textContent = statusInfo;
    if (statusObject) {
        logSpan.append(JSON.stringify(
            statusObject, 
            CompareAction.jobProgressProperties, 
            settings.jsonIndent) + '\n\n');
        logSpan.scrollTop = logSpan.scrollHeight - logSpan.clientHeight;
    }
}

function compareJSON() {
    // initialise CompareAction class
    let c = new CompareAction(settings.deltaJSON);
    c.addProgressListener(progressListener)
        .setPollingInterval(settings.statusPollingInterval)
        .setMaxPollNumber(settings.maxPollNumber);

    // reset HTML page context
    logSpan.textContent = '';
    resultElement.textContent = '';
    let fA: File = inputElementFileA.files[0];
    let fB: File = inputElementFileB.files[0];

    // Setup configuration for the comparison
    let config = new CompareConfig(fA, fB);
    config
        .addIgnoreChanges('myke')
        .addIgnoreChanges('also')
        .setArrayAlignment(ArrayAlignment.orderless)
        .setWordByWord(true);
    
    // Invoke an asynchronous comparison
    c.compare(config).then((data) => {
        resultElement.textContent = JSON.stringify(data, null, settings.jsonIndent);
    })
    .catch((error) => {
        logSpan.append(JSON.stringify(error, null, settings.jsonIndent) + '\n\n');
        logSpan.scrollTop = logSpan.scrollHeight - logSpan.clientHeight;
        statusSpan.textContent = "ERROR";
    });
}
```

_Sample Client Screenshot_

![Screenshot](browser-deltajson.png)

## Converting typescript project to javascript project 

1. Run the project for typescript and build it as described above.
2. Now create a saperate project for javascript.
3. Copy the following files inside the project:
    * index.html
    * main.css
    * out/*.js
    * lib/require.js
4. Change the locations of the scripts in index.html according to your project structure.
5. Run index.html using LiveServer (Link provided in Project Setup).

## API Documentation

API documentation can be generated using [TypeDoc](https://typedoc.org).

From the project directory:
* Run ```npm install -typedoc --save-dev```
* Run ```npx typedoc --out docs src```